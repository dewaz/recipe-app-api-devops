# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "2.50.0"
  constraints = "~> 2.50.0"
  hashes = [
    "h1:c60yYzCoZtgpgYl6LAOqOlAF1IT1oMXfU3QWpX8p9TI=",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:gKYuAb9UyTmIgWLf9i5iYuokWA3QIhdFZkQYgW++NQU=",
  ]
}
