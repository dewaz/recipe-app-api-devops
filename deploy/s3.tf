resource "aws_s3_bucket" "app_public_files" {
  bucket_prefix = "${local.prefix}-files"
  #acl           = "public-read"
  force_destroy = true
}

# The terraform version used in the course is outdated.
#Hopefully the instructor updates the course with a newer version
